# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 14:32:51 2018

@author: User
"""

import pysyzygy as ps
import numpy as np
import matplotlib.pyplot as pl

# Instantiate a transit object
trn = ps.Transit(t0 = 0.5, RpRs = 0.1, per = 1.234) 

# Now evaluate the light curve on a grid of observation times
t = np.arange(0., 3.5, ps.transit.KEPSHRTCAD)
flux = trn(t)

# Plot the light curve
fig, ax = pl.subplots(figsize = (12, 5))
fig.subplots_adjust(bottom = 0.15)
ax.plot(t, flux, 'b.')
ax.set_xlabel('Time (days)', fontsize = 18)
ax.set_ylabel('Relative flux', fontsize = 18)
ax.margins(None, 0.5)
pl.show()