# -*- coding: utf-8 -*-
"""
Created on Sun Jan 27 12:15:08 2019

@author: User
"""

import ephem
msro = ephem.Observer()
msro.lon = '-77:42:39'
msro.lat = '38:20:02'
msro.elevation = 461
msro.date = '2019/1/27'

msro.compute_pressure()
msro.pressure